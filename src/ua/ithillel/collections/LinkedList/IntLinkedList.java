package ua.ithillel.collections.LinkedList;

import ua.ithillel.collections.arraylist.IntIterator;
import ua.ithillel.collections.arraylist.IntList;

public class IntLinkedList implements IntList {

    private Node head;
    private int size;

    public IntLinkedList() {
        head = null;
        size = 0;
    }

    @Override
    public void add(int element) {
        Node newNode = new Node();
        newNode.setValue(element);
        newNode.setNext(null);

        if (head == null) {
            head = newNode;
        } else {
            Node currentNoda = head;
            while (currentNoda.getNext() != null) {
                currentNoda = currentNoda.getNext();
            }
            currentNoda.setNext(newNode);
        }
        size++;
    }

    @Override
    public void add(int index, int element) {
        Node node = head;
        int count = 0;

        while (node != null) {
            if (index - 1 == count) {
                Node previousNode = node;
                Node nextNode = node.getNext();

                Node newNode = new Node();
                newNode.setValue(element);
                newNode.setNext(nextNode);

                previousNode.setNext(newNode);
            }
            count++;
            node = node.getNext();
        }
        size++;
    }

    @Override
    public void clear() {
        head = null;
        size = 0;

    }

    @Override
    public int get(int index) {
        Node node = head;
        for (int i = 0; i < index; i++) {
            node = node.getNext();
        }
        return node.getValue();
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int remove(int index) {
        int valueToBeRemoved = 0;

        if (index == 0) {
            head = head.getNext();
            valueToBeRemoved = head.getValue();
        } else {
            Node node = head;
            int count = 0;

            while (node != null) {
                if (index - 1 == count) {
                    Node previousNode = node;
                    Node nodeToBeRemoved = node.getNext();

                    valueToBeRemoved = nodeToBeRemoved.getValue();
                    previousNode.setNext(nodeToBeRemoved.getNext());
                }

                count++;
                node = node.getNext();
            }
        }

        size--;
        return valueToBeRemoved;
    }

    @Override
    public boolean removeByValue(int value) {
        Node node = head;
        Node previousNode = null;

        while (node != null) {
            if (node.getValue() == value) {
                Node nextNode = node.getNext(); // can be null

                if (previousNode != null) {
                    previousNode.setNext(nextNode);
                } else {
                    head = nextNode;
                }
                size--;
                return true;
            }

            previousNode = node;
            node = node.getNext();
        }
        return false;
    }

    @Override
    public int set(int index, int element) {
        Node node = head;
        int count = 0;
        while (node != null) {
            if (count == index) {
                int previousValue = node.getValue();
                node.setValue(element);
                return previousValue;
            }
            count++;
            node = node.getNext();
        }
        return count;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public IntList subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public int[] toArray() {
        if (head == null) {
            return new int[0];
        } else {
            int[] array = new int[size];
            int count = 0;
            Node node = head;
            while (node != null) {
                array[count] = node.getValue();
                count++;
                node = node.getNext();
            }
            return array;
        }
    }

    private static class Node {

        private int value;
        private Node next;

        public Node() {
        }

        public Node(int value, Node next) {
            this.value = value;
            this.next = next;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public Node getNext() {
            return next;
        }

        public Node setNext(Node next) {
            this.next = next;
            return next;
        }
    }

    @Override
    public IntIterator iterator() {
        return new IntIterator() {
            int position;

            @Override
            public boolean hasNext() {
                for (int i = 0; i < position; i++) {
                    head = head.getNext();
                }
                return head != null;
            }

            @Override
            public int next() {
                Node node = head;
                int value = 0;
                for (int i = 0; i <= position; i++) {
                    if (i != position) {
                        node = node.getNext();
                    } else {
                        value = node.getValue();
                    }
                }
                position++;
                return value;
            }
        };
    }
}



