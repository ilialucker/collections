package ua.ithillel.collections.arraylist;

public interface IntList {

  /**
   * Appends the specified element to the end of this list.
   *
   * @param element element to be appended to this list
   */
  void add(int element);

  /**
   * Inserts the specified element at the specified position in this list. Shifts the element
   * currently at that position (if any) and any subsequent elements to the right (adds one to their
   * indices).
   *
   * @param index   index at which the specified element is to be inserted
   * @param element element to be inserted
   */
  void add(int index, int element);

  /**
   * Removes all of the elements from this list. The list will be empty after this call returns.
   */
  void clear();

  /**
   * Returns the element at the specified position in this list.
   *
   * @param index index of the element to return
   * @return the element at the specified position in this list
   */
  int get(int index);

  /**
   * Returns true if this list contains no elements.
   *
   * @return true if this list contains no elements
   */
  boolean isEmpty();

  /**
   * Removes the element at the specified position in this list (optional operation). Shifts any
   * subsequent elements to the left (subtracts one from their indices). Returns the element that
   * was removed from the list.
   *
   * @param index the index of the element to be removed
   * @return the element that was removed from the list
   */
  int remove(int index);

  /**
   * Removes the first occurrence of the specified element from this list, if it is present. If this
   * list does not contain the element, it is unchanged.
   *
   * @param value element to be removed from this list, if present
   * @return true if this list contained the specified element
   */
  boolean removeByValue(int value);

  /**
   * Replaces the element at the specified position in this list with the specified element.
   *
   * @param index   index of the element to replace
   * @param element element to be stored at the specified position
   * @return the element previously at the specified position
   */
  int set(int index, int element);

  /**
   * Returns the number of elements in this list.
   *
   * @return the number of elements in this list
   */
  int size();

  /**
   * Returns a view of the portion of this list between the specified fromIndex, inclusive, and
   * toIndex, exclusive.
   *
   * @param fromIndex low endpoint (inclusive) of the subList
   * @param toIndex   high endpoint (exclusive) of the subList
   * @return a view of the specified range within this list
   */
  IntList subList(int fromIndex, int toIndex);

  /**
   * Returns an array containing all of the elements in this list in proper sequence (from first to
   * last element).
   *
   * @return an array containing all of the elements in this list in proper sequence
   */
  int[] toArray();

  IntIterator iterator();

}
