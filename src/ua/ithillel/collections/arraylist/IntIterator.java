package ua.ithillel.collections.arraylist;

public interface IntIterator {

    boolean hasNext();

    int next();
}
