package ua.ithillel.collections.arraylist;

import java.util.Arrays;

public class IntArrayList implements IntList {

  private int[] elementData;

  public IntArrayList() {
    elementData = new int[0];
  }

  @Override
  public void add(int element) {
    elementData = Arrays.copyOf(elementData, elementData.length + 1);
    elementData[elementData.length - 1] = element;
  }

  @Override
  public void add(int index, int element) {
    int[] newArr = new int[elementData.length + 1];

    System.arraycopy(elementData, 0, newArr, 0, index);
    newArr[index] = element;
    System.arraycopy(elementData, index, newArr, index + 1, elementData.length - index);
    elementData = newArr;
  }

  @Override
  public void clear() {
    elementData = new int[0];
  }

  @Override
  public int get(int index) {
    return elementData[index];
  }

  @Override
  public boolean isEmpty() {
    return elementData.length == 0;
  }

  @Override
  public int remove(int index) {
    int valueToBeRemoved = elementData[index];
    int[] newElementsData = new int[elementData.length - 1];

    System.arraycopy(elementData, 0, newElementsData, 0, index);
    System.arraycopy(elementData, index + 1, newElementsData, index, elementData.length - index - 1);
    elementData = newElementsData;

    return valueToBeRemoved;
  }

  @Override
  public boolean removeByValue(int value) {
    for (int i = 0; i < elementData.length; i++) {
      if (elementData[i] == value) {
        this.remove(i);
        return true;
      }
    }
    return false;
  }

  @Override
  public int set(int index, int element) {
    int previousElement = elementData[index];
    elementData[index] = element;
    return previousElement;
  }

  @Override
  public int size() {
    return elementData.length;
  }

  @Override
  public IntList subList(int fromIndex, int toIndex) {
    IntList subList = new IntArrayList();
    for (int i = fromIndex; i < toIndex; i++) {
      subList.add(elementData[i]);
    }
    return subList;
  }

  @Override
  public int[] toArray() {
    return elementData;
  }

  @Override
  public IntIterator iterator() {
    return new IntIterator() {
      private int currentIndex = 0;

      @Override
      public boolean hasNext() {
        return currentIndex != elementData.length;
      }

      @Override
      public int next() {
        return elementData[currentIndex++];
      }
    };
  }
}


